package main

import (
  "bufio"
  "encoding/base64"
  "fmt"
  "io/ioutil"
  "os"
  "os/exec"
  "path/filepath"
  "runtime"
  "strings"

  "github.com/mattn/go-tty"
)

func getHibroPath() string {
  homePath, err := os.UserHomeDir()
  if err != nil {
    fmt.Println(err)
    os.Exit(0)
  }
  if runtime.GOOS == "windows" {
    return filepath.Join(homePath, "AppData", "Local", "hibro")
  } else {
    return filepath.Join(homePath, ".config", "hibro")
  }
}

var hibropath string = getHibroPath()
var targets []string

// check if hibropath exist and create it if not
func createHibroFolder() {
  if _, err := os.Stat(hibropath); os.IsNotExist(err) {
    os.Mkdir(hibropath, 0755)
  }
}

// sync repos in hibrorc
func syncRepos() error {
  file, err := os.Open(filepath.Join(hibropath, "hibrorc"))
  if err != nil {
    return err
  }
  scanner := bufio.NewScanner(file)
  var repos []string
  for scanner.Scan() {
    if strings.Contains(scanner.Text(), "sync=") {
      repos = append(repos, strings.ReplaceAll(scanner.Text(), "sync=", ""))
    }
  }
  defer file.Close()

  for i := 0; i < len(repos); i++ {
    cmd := exec.Command("git", "clone", repos[i], filepath.Join(filepath.Join(hibropath, "sync"), strings.ReplaceAll(strings.Split(repos[i], "/")[3], "\n", " ")))
    cmd.Run()
  }
  return nil
}

// execute payload on targets
func execPayload(command string) {
  var commands []string = strings.Split(command, " ")
  var payload string
  if len(commands) < 2 {
    var owner string = strings.Split(commands[0], "/")[0]
    var payloadname string = strings.Split(commands[0], "/")[1]

    payload, err := ioutil.ReadFile(filepath.Join(hibropath, "sync", owner, payloadname))
    if err != nil {
      fmt.Println("error on loading payload")
    }
    var encodedpayload string = base64.StdEncoding.EncodeToString([]byte(payload))
    if encodedpayload == "" {
    } // acordarse de borrar este if cuando se use encodedpayload
    for i := 0; i < len(targets); i++ {
      // send encodedpayload to everytarget
    }
  } else {
    var owner string = strings.Split(command, "/")[0]
    var payloadname string = strings.Split(command, "/")[1]
    var target string = strings.Split(command, " ")[1]
    if target == "" {
    } // acordarse de borrar este if cuando se use
    if owner == "local" {
      payload, err := ioutil.ReadFile(filepath.Join(hibropath, owner, payloadname))
      if payload == nil {
      } // acordarse de borrar este if cuando se use
      if err != nil {
        fmt.Println("error on loading payload")
      }
    } else {
      payload, err := ioutil.ReadFile(filepath.Join(hibropath, "sync", owner, payloadname))
      if payload == nil {
      } // acordarse de borrar este if cuando se use
      if err != nil {
        fmt.Println("error on loading payload")
      }
    }
    var encodedpayload string = base64.StdEncoding.EncodeToString([]byte(payload))
    if encodedpayload == "" {
    } // acordarse de borrar este if cuando se use
    // send encodedpayload to specific target
  }
}

// stop execution
func quit() {
  os.Exit(0)
}

// print help
func help() {
  fmt.Println(":sync              Clone all git repositories with the payloads into ~/.config/hibro/payloads/ | WARNING: It will empty the folder first")
  fmt.Println()
  fmt.Println(":list_targets      List all connected targets")
  fmt.Println()
  fmt.Println(":q                 Exit program closing the sockets")
}

// clear terminal
func clear() {
  for i := 0; i < 1000; i++ {
    fmt.Println()
  }
  // cmd.Stdout = os.Stdout
}

// fill an array with content to be displayed as menu
func fillMenu() []string {
  var menucontent []string
  var payloads []string

  //local payloads
  if _, err := os.Stat(filepath.Join(hibropath, "payload")); os.IsNotExist(err) {
    os.Mkdir(filepath.Join(hibropath, "payload"), 0755)
  }
  localpayloads, err := ioutil.ReadDir(filepath.Join(hibropath, "payloads"))
  if err == nil {
    for _, f := range localpayloads {
      payloads = append(payloads, ("local/" + f.Name()))
    }
  }

  //synced payloads
  syncrepos, err := ioutil.ReadDir(filepath.Join(hibropath, "sync"))
  if err == nil {
    for _, syncrepo := range syncrepos {
      syncpayloads, err := ioutil.ReadDir(filepath.Join(hibropath, "sync", syncrepo.Name(), "payload"))
      if err == nil {
        for _, syncpayload := range syncpayloads {
          payloads = append(payloads, (syncrepo.Name() + "/" + syncpayload.Name()))
        }
      }
    }
  }

  // concat payloads to menucontent
  menucontent = append(menucontent, payloads...)

  //commands
  menucontent = append(menucontent, "")
  menucontent = append(menucontent, ":help")
  menucontent = append(menucontent, ":sync")
  menucontent = append(menucontent, ":list_targets")
  menucontent = append(menucontent, ":q")

  return menucontent
}

//todo
func menu() {
  clear()

  var menucontent []string = fillMenu()

  var command string = ""
  var screenmenu []string
  for true {
    screenmenu = []string{}
    if command == "" {
      for i := 0; i < len(menucontent); i++ {
        screenmenu = append(screenmenu, menucontent[i])
      }
    } else {
      for i := 0; i < len(menucontent); i++ {
        if strings.Contains(menucontent[i], command) {
          screenmenu = append(screenmenu, menucontent[i])
        }
      }
    }

    fmt.Println()
    for i := 0; i < len(screenmenu); i++ {
      fmt.Println(screenmenu[i])
    }

    // insert part of the menu
    fmt.Println()
    fmt.Println("write 'help' to get the list of available commands and their description")
    fmt.Print("hibro: " + command)

    // listen keyboard press
    tty, err := tty.Open()
    if err != nil {
      fmt.Println(err)
    }
    defer tty.Close()
    r, err := tty.ReadRune()
    if err != nil {
      fmt.Println(err)
    }
    var stdin string = string(r)

    // clear
    clear()

    // validate input
    if "\n" == stdin {
      if len(command) == 0 {
        break
      } else if command == ":help" {
        help()
      } else if command == ":sync" {
        syncRepos()
      } else if command == ":list_targets" {
        for i := 0; i < len(targets); i++ {
          fmt.Println(targets[i])
        }
      } else if command == ":q" {
        quit()
      } else {
        execPayload(command)
      }
      command = ""
    } else if "\x1b" == stdin { // esc
      command = ""
    } else if "\t" == stdin { // tab
      for i := 0; i < len(menucontent); i++ {
        if strings.Contains(menucontent[i], command) {
          command = menucontent[i]
          break
        }
      }
    } else if "\x7f" == stdin { // backspace
      command = command[:len(command)-1]
    } else { // add all other characters to command
      command += stdin
    }

  }
}

func main() {
  createHibroFolder()
  err := syncRepos()
  if err != nil {
    fmt.Println(err)

  }
  menu()
}
