import asyncio
import websockets
import os
from sys import platform
import getch
import base64
from multiprocessing import Pool

targets = []


async def handler(websocket):
    while True:
        message = await websocket.recv()
        # todo: send message to target log file
        print(message)

# async def send(uri, data):
    # async with websocket.connect(uri) as websocket:
        # await websocket.send(data)

# paths
if platform == "linux":
    configdirpath = os.getenv('XDG_CONFIG_HOME')
    if(configdirpath is None):
        configdirpath = os.path.expanduser('~')+'/.config'
if platform == "win32":
    configdirpath = os.getenv('APPDATA')
configpath = configdirpath+'/hibro'

# print help


def help():
    print("""
    :sync              Clone all git repositories with the payloads into ~/.config/hibro/payloads/ | WARNING: It will empty the folder first
    :list_targets      List all connected targets
    :q                 Exit program closing the sockets
    """)

# download all repositories with payloads written in config
# all repositories must contains the payloads inside a folder called 'payloads'


repos = []


def sync():
    if os.path.exists(configpath+'/config'):
        rc = open(configpath+'/config', 'r')
        lines = rc.readlines()
        for line in lines:
            if 'sync=' in line:
                repos.append(line.split('sync=')[1])
        rc.close()

    syncpath = configpath+'/sync'
    if os.path.exists(syncpath):
        os.rmdir(syncpath)
        os.mkdir(syncpath)
        for repo in repos:
            os.system(('git clone ' + repo + ' ' + syncpath +
                      '/' + repo.split('/')[3]).replace('\n', ''))

# execute payload on targets


def exec(command):
    commands = command.split()
    if(len(commands) < 2):  # execute command for all active targets
        owner = commands.split()[0].split('/')[0]
        payloadname = commands.split()[0].split('/')[1]
        payload = open(configpath+'/sync/'+owner+'/'+payloadname, 'r').read()
        encodedpayload = base64.b64encode(payload.encode("utf-8"))
        for target in targets:
            # sending message ...
            asyncio.get_event_loop().create_task(websockets.send(target, encodedpayload))

    else:  # execute command for specific target
        owner = commands.split('/')[0]
        payloadname = commands.split('/')[1]
        if(owner == 'local'):
            payload = open(configpath+'/'+owner+'/'+payloadname, 'r').read()
        else:
            payload = open(configpath+'/sync/'+owner +
                           '/'+payloadname, 'r').read()
        encodedpayload = base64.b64encode(payload.encode("utf-8"))
        # sending message ...
        asyncio.get_event_loop().create_task(
            websockets.send(commands.split('/')[1], encodedpayload))

# clear the terminal


def clear():
    for i in range(200):
        print('')


# create array with the menu to display
menu_content = []


def menucontent():
    menu_content.clear()
    # add payloads to menu
    payloads = []
    # local payloads
    if os.path.exists(configpath+'/payload'):
        for payload in os.listdir(configpath + "/payload"):
            payloads.append("local/" + payload)

    # synced payloads
    if os.path.exists(configpath+"/sync"):
        for repo in os.listdir(configpath+"/sync"):
            for payload in os.listdir(configpath + "/sync/" + repo + "/payload"):
                payloads.append(repo + "/" + payload)

    for payload in payloads:
        menu_content.append(payload)

    # add commands to menu
    menu_content.append('')
    menu_content.append(':help')
    menu_content.append(':sync')
    menu_content.append(':list_targets')
    menu_content.append(':q')


# main menu
current_selection = []

# cyclomatic complexity is over 9000, I know.


def menu():
    # clear()

    menucontent()

    command = ''
    while True:
        # print menu content
        current_selection.clear()
        if(command == ''):
            for content in menu_content:
                current_selection.append(content)
        else:
            for content in menu_content:
                if(command in content):
                    current_selection.append(content)

        for content in current_selection:
            print(content)

        # insert part of the menu
        print('')
        print('write "help" to get the list of available commands and their description')
        print('hibro: ' + command)
        stdin = getch.getch()

        clear()

        # validate input
        if("\n" == stdin):  # Enter
            if len(command) == 0:
                pass
            elif command == ':help':
                help()
            elif command == ':sync':
                sync()
                menucontent()
            elif command == ':list_targets':
                for target in targets:
                    print(target)
            elif command == ':q':
                quit()
            else:
                exec(command)
            command = ''
        elif ("\x1b" == stdin):  # Esc
            command = ''
        elif ("\t" == stdin):  # tab
            for content in menu_content:
                if(command in content):
                    command = content
                    break
        elif ("\x7f" == stdin):  # Backspace
            command = command[:-1]
        else:
            command += stdin


def main():
    # load config if exists
    sync()
    # run websocket
    with Pool() as pool:
        pool.imap_unordered(websockets.serve(handler, "localhost", 8090), range(1))
    # run program menu
    menu()


if __name__ == "__main__":
    main()
